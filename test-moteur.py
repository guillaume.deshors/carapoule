from machine import Pin, ADC #, deepsleep
from time import sleep, ticks_ms, ticks_diff


# constants
PIN_ENABLE = 9
PIN_LPWM = 8
PIN_RPWM = 7

PIN_LIMIT0 = 0
PIN_LIMIT1 = 1


led = Pin(25, Pin.OUT)

enable = Pin(PIN_ENABLE, Pin.OUT)
lpwm = Pin(PIN_LPWM, Pin.OUT)
rpwm = Pin(PIN_RPWM, Pin.OUT)

limit0 = Pin(PIN_LIMIT0, Pin.IN, Pin.PULL_UP)
limit1 = Pin(PIN_LIMIT1, Pin.IN, Pin.PULL_UP)

enable.value(1)
led.value(1)
lpwm.value(1)

while True:
    lpwm.value(limit0.value())
    rpwm.value(limit1.value())