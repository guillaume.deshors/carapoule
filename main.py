from machine import Pin, ADC, PWM 
from time import sleep, ticks_ms, ticks_diff

# algo
# read luminosity every 5s and mean the last 3 values
# if above night threshold : open ; else close

print("OK version 0.9 - debounce on limits")

# constants


# EVENING
LIGHT_LIMIT = 2000

PIN_ENABLE = 9
PIN_LPWM = 8
PIN_RPWM = 7
PIN_LIMIT0 = 1
PIN_LIMIT1 = 0
PIN_BUTTON = 14
PIN_PHOTORES = 26

TIME_ERROR_MS = 15000

# duty cycle for the motor (controlling speed)
# 0 is stopped, 65535 is full
MOTOR_DUTY_CYLE = 20000

# Variables

day = True

led = Pin(25, Pin.OUT)
enable = Pin(PIN_ENABLE, Pin.OUT)
lpwm = PWM(Pin(PIN_LPWM, Pin.OUT))
rpwm = PWM(Pin(PIN_RPWM, Pin.OUT))
lpwm.freq(25000)
rpwm.freq(25000)
# upper limit
upper_limit = Pin(PIN_LIMIT0, Pin.IN, Pin.PULL_UP)
# lower limit
lower_limit = Pin(PIN_LIMIT1, Pin.IN, Pin.PULL_UP)
button = Pin(PIN_BUTTON, Pin.IN, Pin.PULL_UP)

photoRes = ADC(Pin(PIN_PHOTORES))

button_pressed = False

# define functions

# return true if sensor is active (== pressed)
def is_active(sensor):
    return sensor.value() == 0

# return true if sensor is inactive (== not pressed)
def is_inactive(sensor):
    return sensor.value() == 1

# an error happened : blink rapidly forever
def error_exit():
    print("error - failsafe state")
    enable.value(0)
    lpwm.duty_u16(0)
    rpwm.duty_u16(0)
    while (True):
        led.toggle()
        sleep(0.05)


def button_pressed_i(toto):
    global button_pressed
    # debounce and avoid 'ghost presses' (EMI ?)
    time = ticks_ms()
    for i in range(0, 10000):
        if (button.value() == 1):
            break
    if (ticks_diff(ticks_ms(), time) > 50):
        print("********** button *************")
        button_pressed = True


def open_door():
    global button_pressed
    global upper_limit
    global lower_limit
    if (is_active(upper_limit)):
        print("not opening the door - upper sensor is active")
    else:
        print("opening the door")
        # we can start opening
        start = ticks_ms()
        enable.value(1)
        rpwm.duty_u16(MOTOR_DUTY_CYLE)
        lower_has_been_inactive = False
        while (is_inactive(upper_limit) and not button_pressed):
            if (is_inactive(lower_limit)):
                lower_has_been_inactive = True
                # debounce
                sleep(0.03)
            if (lower_has_been_inactive and is_active(lower_limit)):
                print("we were going up but the lower limit has activated - going up again to detangle")
                # we were supposedly going up, but the lower limit has activated
                # we should continue to run the motor ; the thread will invert and the door will go up
                # let's reset the timer so we have time to go full up
                start = ticks_ms()
                # but let's also reset lower_has_been_inactive so we don't reset the timer indefinitely (for example if the thread is broken)
                lower_has_been_inactive = False
            if (ticks_ms() > start + TIME_ERROR_MS):
                # error : limit switch not reached in time
                # stop motor
                rpwm.duty_u16(0)
                sleep(1)
                error_exit()
        # OK limit reached
        print("end of opening the door")
        button_pressed = False
        rpwm.duty_u16(0)
        sleep(1)
        enable.value(0)


def close_door():
    global button_pressed
    global upper_limit
    global lower_limit
    if (is_active(lower_limit)):
        print("not closing the door ; the lower sensor is active")
    else:
        print("closing the door")
        # we can start opening
        start = ticks_ms()
        enable.value(1)
        lpwm.duty_u16(MOTOR_DUTY_CYLE)
        upper_has_been_inactive = False
        while (is_inactive(lower_limit) and not button_pressed):
            if (is_inactive(upper_limit)):
                upper_has_been_inactive = True
                # debounce
                sleep(0.03)
            if (upper_has_been_inactive and is_active(upper_limit)):
                print("we were going down but the upper limit has activated - reversing the motor then telling the door to 'open'")
                # we were supposedly going down, but the upper limit has activated
                # we should tell the door to _open_, and the door should go down again.
                # But before that, let's reverse the motor until we free the sensor
                # 1 : stop
                lpwm.duty_u16(0)
                sleep(1)
                # 2 : reverse
                rpwm.duty_u16(MOTOR_DUTY_CYLE)
                # 3 : wait until upper limit goes inactive (check the time for security)
                while (is_active(upper_limit) and ticks_ms() < start + TIME_ERROR_MS):
                    sleep(0.1)
                print("ok the sensor has gone inactive")
                # 4 : stop the motor
                rpwm.duty_u16(0)
                # 5 : 'open' door (it will really close)
                open_door()
                # 6 : break so we stop the initial cycle
                break
            if (ticks_ms() > start + TIME_ERROR_MS):
                # error : limit switch not reached in time
                # stop motor
                lpwm.duty_u16(0)
                sleep(1)
                error_exit()
        # OK limit reached or button pressed
        print("end of closing the door")
        button_pressed = False
        lpwm.duty_u16(0)
        sleep(1)
        enable.value(0)


# début

button.irq(trigger=Pin.IRQ_FALLING, handler=button_pressed_i)

led.value(0)
enable.value(0)

# init light array
light = photoRes.read_u16()
nb = 0

while True:

    # read light every few seconds
    light = photoRes.read_u16()
    print("light = " + str(light))

    led.toggle()
    sleep(0.05)
    led.toggle()
    if (light > LIGHT_LIMIT):
        sleep(0.05)
        led.toggle()
        sleep(0.05)
        led.toggle()
        if (day):
            nb = 0
        else:
            nb = nb + 1
            if (nb == 3):
                # switch to day
                print("morning...")
                day = True
                nb = 0
                # open door
                open_door()
    else:  # low light
        if (not day):
            nb = 0
        else:
            nb = nb + 1
            if (nb == 3):
                # switch to night
                print("evening...")
                day = False
                nb = 0
                # close door
                close_door()

    print("wait 3 sec")
    # wait 3s
    start = ticks_ms()
    time = start
    while (ticks_diff(time, start) < 3000):
        time = ticks_ms()
        if (button_pressed):
            print("button pressed -> opening or closing the door")
            button_pressed = False
            if (day):
                day = False
                close_door()
            else:
                day = True
                open_door()
